# Django Tutorial

This is my first django project

## Setting up a new environment
Before we do anything else we'll create a new virtual environment, using django_venv. This will make sure our package configuration is kept nicely isolated from any other projects we're working on.

```bash
python3 -m venv django_env
source django_env/bin/activate
```

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the required libraries which are there in requirements.txt file.

```bash
pip install -r requirements.txt
```
Now sync your database for the first time:
```python
python manage.py migrate
```
## Testing our API
We're now ready to test the API we've built. Let's fire up the server from the command line.

```python
python manage.py runserver
```